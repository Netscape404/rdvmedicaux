# RDVMEDICAUX
## Commandes

```
php bin/console make:entity
```
### Champs
* message       : text
* note          : int
* valide        : boolean
* dateEnvoi    : datetime

```
php bin/console make:migration
```

```
php bin/console doctrine:migrations:migrate
```