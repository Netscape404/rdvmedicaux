<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RdvRepository")
 */
class Rdv
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creneau;

    /**
       * @ORM\ManyToOne(targetEntity="Medecin", inversedBy="horaires")
       * @ORM\JoinColumn(name="medecin_id", referencedColumnName="id")
       */
      private $medecin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreneau(): ?\DateTimeInterface
    {
        return $this->creneau;
    }

    public function setCreneau(\DateTimeInterface $creneau): self
    {
        $this->creneau = $creneau;

        return $this;
    }
}
